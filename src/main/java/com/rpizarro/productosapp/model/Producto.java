package com.rpizarro.productosapp.model;

public class Producto {

	private int id;
	private String nombre;
	private String marca;
	private int stock;
	private int precio;

	public Producto(int id, String nombre, String marca, int stock, int precio) {
		this.id = id;
		this.nombre = nombre;
		this.marca = marca;
		this.stock = stock;
		this.precio = precio;
	}

	public Producto() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

}
