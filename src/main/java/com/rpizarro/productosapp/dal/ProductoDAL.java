package com.rpizarro.productosapp.dal;

import java.util.ArrayList;

import com.rpizarro.productosapp.model.Producto;

public class ProductoDAL {

	ArrayList<Producto> productos = new ArrayList<>();
	
	public void add(Producto p) {
		productos.add(p);
	}
	
	public ArrayList<Producto> getAll(){
		return productos;
	}
	
	public Producto findById(int id) {
		Producto p = null;

		for (Producto aux : productos) {
			if (aux.getId() == id) {
				p = aux;
			}
		}
		
		return p;
	}
	
	public boolean delete(int id) {
		boolean result = false;
		int indice = 0;
		int posicion = -1;
		
		for (Producto producto : productos) {
			if (producto.getId() == id) {
				posicion = indice;
			}
			indice++;
		}
		
		if (posicion != -1) {
			result = true;
			productos.remove(posicion);
		}
		
		
		return result;
	}
	
	public Producto update(Producto p) {
		boolean encontrado=false;;
		int i=0, posicion=-1;
		Producto modificado = null;
		for (Producto aux : productos) {
			if (aux.getId() == p.getId()) {
				posicion = i;
				encontrado = true;
			}
         i++;
		}
		
		if(encontrado) {
			modificado = productos.get(posicion);
			
			modificado.setNombre(p.getNombre());
			modificado.setMarca(p.getNombre());
			modificado.setStock(p.getStock());
			modificado.setPrecio(p.getPrecio());
		}
		
		
		return modificado;
	}
	
	
}
