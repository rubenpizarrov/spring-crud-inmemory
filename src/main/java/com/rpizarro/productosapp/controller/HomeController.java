package com.rpizarro.productosapp.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.rpizarro.productosapp.dal.ProductoDAL;
import com.rpizarro.productosapp.model.Producto;

@Controller
public class HomeController {
	
	ProductoDAL proDal = new ProductoDAL();
	
	/**
	 * Ruta de Inicio
	 * @return
	 */
	@GetMapping("/")
	public String index() {
		return "home";
	}
	
	/**
	 * LLamada a la vista de creación
	 * o Insert
	 * @return vista crear
	 */
	@GetMapping("/crear")
	public String crear() {
		return "crear";
	}
	
	/**
	 * Procesa formulario /crear
	 * agrega un Object al Arraylist
	 * @return vista crear
	 */
	
	@PostMapping("/registrar")
	public String procesaForm(Producto p, Model model) {
		Producto prod = p;
		proDal.add(prod);
		model.addAttribute("msg", "guardado exitosamente");
		return "/crear";
	}
	
	
	/**
	 * Permite buscar para editar o eliminar
	 * @return vista buscar
	 */
	@GetMapping("/buscar")
	public String buscarView() {
		return "buscar";
	}
	
	/**
	 * Procesa el formulario de la vista
	 * /buscar
	 * @return vista Model /buscar
	 */
	@PostMapping("/buscarid")
	public String buscarId(@RequestParam(name="id",required = true, defaultValue="0") int id, Model model) {
		
		Producto prod = proDal.findById(id);
		
		String msg ="";
		
		if(prod == null)
			msg = "No existe el Producto indicado";
		
		model.addAttribute("producto", prod);
		model.addAttribute("msg", msg);
		
		return "buscar";
	}
	
	/**
	 * LLamada a la vista
	 * /listar con la grilla de objetos
	 * @return vista /listar
	 */
	@GetMapping("/listar")
	public String listar(Model model) {
		ArrayList<Producto> listaProd = proDal.getAll();
		model.addAttribute("lista", listaProd);
		return "listar";
	}
	
	/**
	 * Procesa el formulario de la vista
	 * /buscar al presionar boton Editar
	 * @return vista Model /actualizar?id=int
	 */
	@GetMapping(path={"/actualizar", "/actualizar/{id}"})
	public String editarProducto(@RequestParam(name="id", required = true, defaultValue = "0") int id, Model model) {
		
		Producto prod = proDal.findById(id);
		String msg ="";
		
		
		if(prod == null)
			msg = "No existe el Producto indicado";
		
		model.addAttribute("producto", prod);
		model.addAttribute("msg", msg);
		
		return "actualizar";
	}
	
	/**
	 * Procesa el formulario de la vista
	 * /actualizar?id=int
	 * @return vista Model actualizado
	 */
	@PostMapping("/procesaActualizar")
	public String procesaEditar(Producto p, Model model) {
		Producto prod;
		if (proDal.update(p) != null) {
			prod = proDal.update(p);
			model.addAttribute("msg", "Guardado exitósamente");
			model.addAttribute("producto", prod);
		} else {
			model.addAttribute("msg", "No se encontró el producto");
		}
		
		return "actualizar";
	}
	
	/**
	 * Procesa el formulario de la vista
	 * /buscar elimina por ID
	 * @return void redirecciona a la vista /buscar
	 */
	@GetMapping(path={"/eliminar", "/eliminar/{id}"})
	public String eliminarProducto(@RequestParam(name="id", required = true, defaultValue = "0") int id, Model model) {
		boolean result = proDal.delete(id);

		if (result) {
			model.addAttribute("msg", "Se eliminó el producto");
		} else {
			model.addAttribute("msg", "El producto no existe o no se pudo eliminar");
		}
		
		return "buscar";
	}
	
	

}
