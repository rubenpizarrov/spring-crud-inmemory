package com.rpizarro.productosapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductosappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductosappApplication.class, args);
	}

}
