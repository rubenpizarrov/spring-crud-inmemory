<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="head.jsp"></jsp:include>
    <body>
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <jsp:include page="navbar.jsp"></jsp:include>
                    </div>
                </div>
          </div>
    </body>
</html>
