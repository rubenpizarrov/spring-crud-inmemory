<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="head.jsp"></jsp:include>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<jsp:include page="navbar.jsp"></jsp:include>
			</div>
		</div>
		<div class="row justify-content-center mt-3">
			<div class="col-md-6 ">
				<div class="alert alert-info" role="alert">${msg}</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-6 ">
				<h1 class="text-center">Registro de Productos</h1>
				<form action="/buscarid" method="POST">
					<div class="form-group">
						<label for="txtId">Id</label> <input type="text"
							class="form-control" id="txtId" name="id">
					</div>
					<button type="submit" class="btn btn-danger">Buscar</button>
					<div class="row">
						<div class="col-md-12">
							<h1 class="text-center">Productos</h1>
						</div>
					</div>
					<div class="form-group">
						<label for="txtNombre">Nombre</label> <input type="text"
							class="form-control" id="txtNombre" name="nombre" value="${producto.nombre}" disabled>
					</div>
					<div class="form-group">
						<label for="txtMarca">Marca</label> <input type="text"
							class="form-control" id="txtMarca" name="marca" value="${producto.marca}" disabled>
					</div>
					<div class="form-group">
						<label for="txtStock">Stock</label> <input type="text"
							class="form-control" id="txtStock" name="stock" value="${producto.stock}" disabled>
					</div>
					<div class="form-group">
						<label for="txtPrecio">Precio</label> <input type="text"
							class="form-control" id="txtPrecio" name="precio" value="${producto.precio}" disabled>
					</div>
					<div class="form-group">
						<a href="actualizar?id=${producto.id}" class="btn btn-warning" role="button">Editar</a>
						<a href="eliminar?id=${producto.id}" class="btn btn-danger" role="button">Eliminar</a>
					</div>
				</form>
			</div>
		</div>

	</div>
</body>
</html>
