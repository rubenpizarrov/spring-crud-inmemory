<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="head.jsp"></jsp:include>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<jsp:include page="navbar.jsp"></jsp:include>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-md-6 ">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">Nombre</th>
							<th scope="col">Marca</th>
							<th scope="col">Stock</th>
							<th scope="col">Precio</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${lista}" var="aux">
							<tr>
								<td><c:out value="${aux.getId()}" /></td>
								<td><c:out value="${aux.getNombre()}" /></td>
								<td><c:out value="${aux.getMarca()}" /></td>
								<td><c:out value="${aux.getStock()}" /></td>
								<td><c:out value="${aux.getPrecio()}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</body>
</html>
