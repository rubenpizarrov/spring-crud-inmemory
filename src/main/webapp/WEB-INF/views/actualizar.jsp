<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="head.jsp"></jsp:include>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<jsp:include page="navbar.jsp"></jsp:include>
			</div>
		</div>
		<div class="row justify-content-center mt-3">
			<div class="col-md-6 ">
				<div class="alert alert-info" role="alert">${msg}</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-6 ">
				<h1 class="text-center">Actualizar Productos</h1>
				<form action="/procesaActualizar" method="POST">
					<div class="form-group">
						<label for="txtId">Id</label> <input type="text"
							class="form-control" id="txtId" value="${producto.id}" disabled>
					</div>
					<div class="form-group">
						<label for="txtNombre">Nombre</label> <input type="text"
							class="form-control" id="txtNombre" name="nombre" value="${producto.nombre}">
					</div>
					<div class="form-group">
						<label for="txtMarca">Marca</label> <input type="text"
							class="form-control" id="txtMarca" name="marca" value="${producto.marca}">
					</div>
					<div class="form-group">
						<label for="txtStock">Stock</label> <input type="text"
							class="form-control" id="txtStock" name="stock" value="${producto.stock}">
					</div>
					<div class="form-group">
						<label for="txtPrecio">Precio</label> <input type="text"
							class="form-control" id="txtPrecio" name="precio" value="${producto.precio}">
					</div>
					
					<input type="hidden" name="id" value="${producto.id}">
					
					<button type="reset" class="btn btn-warning">Limpiar</button>
					<button type="submit" class="btn btn-danger"
						value="Registrar">Guardar</button>
				</form>
			</div>
		</div>
	</div>
	
</body>
</html>
